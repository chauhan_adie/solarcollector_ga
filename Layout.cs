using System;
namespace Example.TravellingSalesman
{
	public class Layout
	{
		public Layout(string name, double r, double theta)
		{
			Name = name;
			R = r;
			Theta = theta;
		}

		public string Name { set; get; }

		public double R { set; get; }

		public double Theta { set; get; }

		public double GetDistanceFromOrigin(double r, double theta)
		{
			//d = √( r12 + r22 -2r1r2 cos(Φ2 - Φ1) )
			//Where, d = Distance
			//r1, r2 = Polar coordinate
			//Φ1, Φ2 = Angle
			var OriginR = 0;
			var OriginTheta = 0;

			var d = 0;
			return d;
		}
	}
}
